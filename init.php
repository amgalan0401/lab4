<?php
$products = array(
    array(
        'id'=>1,
        'name'=> 'GUCCI',
        'price'=>  100000,
        'description'=>'Amgalan'
    ),
    array(
        'id'=>2,
        'name'=> 'PRADA',
        'price'=>  100001,
        'description'=>'Gunbee'
    ),
    array(
        'id'=>3,
        'name'=> 'LOUIS VUITTON',
        'price'=>  11,
        'description'=>'Nomio'
    ),
);
$xml =new DOMDocument('1.0','UTF-8');

$root = $xml->createElement('products');
$xml->appendChild($root);

foreach ($products as $value ){
    $product = $xml->createElement('product');
    foreach ($value as $key=>$value){
        $node = $xml->createElement($key,$value);
        $product->appendChild($node);
    }
    $root->appendChild($product);

}
$xml->formatOutput = true;
$xml->save('files/data.xml')or die('Error');
